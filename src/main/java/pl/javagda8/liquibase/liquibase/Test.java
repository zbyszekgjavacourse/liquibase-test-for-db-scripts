package pl.javagda8.liquibase.liquibase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

@Component
public class Test {

    @Autowired
    public DataSource dataSource;

    @PostConstruct
    public void test(){
        System.out.println(dataSource);
    }
}
