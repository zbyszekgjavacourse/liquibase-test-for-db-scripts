package pl.javagda8.liquibase.liquibase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class LiquibaseApplication {



    public static void main(String[] args) {
        SpringApplication.run(LiquibaseApplication.class, args);
    }
}
